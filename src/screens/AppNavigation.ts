// @ts-ignore
import {createStackNavigator, createAppContainer, createSwitchNavigator} from "react-navigation";
import Login from "../containers/Login";
import HomeView from "./home";
import {AppRoute} from "../config/route";


const AppNavigation = createSwitchNavigator({
    [AppRoute.LOGIN]: {
        screen: HomeView
    },
    [AppRoute.LOGOUT]: {
        screen: Login
    }
}, {
    initialRouteName: AppRoute.LOGOUT
});

const AppContainer = createAppContainer(AppNavigation);

export default AppContainer;
