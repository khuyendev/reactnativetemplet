import React from 'react'
import {View, TextInput, Button} from 'react-native';
import styles from './styles'
import TextView from '../../components/TextView';

interface Props {
    logging: boolean
    reduxMgs: string
    message: string

    login(email: string, password: string): any
}

interface State {
    email: string
    password: string

    [key: string]: any
}

export  default class LoginView extends React.PureComponent<Props, State> {
    handleLogin = () => {
        this.props.login("aaa", "aaa");
    };

    handleChangeText = (text: string, key: string) => {
        this.setState({
            [key]: text
        });
    };

    render() {
        return (
            <View style={{
                flex: 1, justifyContent: 'center',
                alignItems: 'center',
            }}>
                <TextView>{this.props.reduxMgs}
                </TextView>
                <TextInput
                    placeholder="Email"
                    onChangeText={(text) => {
                        this.handleChangeText(text, 'email')
                    }}
                    underlineColorAndroid="transparent"
                    style={[styles.formControl]}
                />
                <TextInput
                    placeholder="Password"
                    onChangeText={(text) => {
                        this.handleChangeText(text, 'password')
                    }}
                    underlineColorAndroid="transparent"
                    style={styles.formControl}
                />
                <Button
                    title="Login"
                    disabled={this.props.logging}
                    onPress={this.handleLogin}/>
                <TextView>{this.props.message}</TextView>
            </View>
        )
    }
}