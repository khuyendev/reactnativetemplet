import {connect} from 'react-redux';
import LoginView from '../screens/login/LoginView'
import {requestLogin} from "../redux/actions/loginActions";

const mapStateToProps = (state: any) => ({
    logging: state.auth.logging,
    loadingText: state.auth.loadingText,
    message: state.auth.message
});
const mapDispatchToProps = (dispatch: any) => ({
    login: (id: string, password: string) => dispatch(requestLogin(id, password))
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginView)