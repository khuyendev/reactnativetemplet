import { theme } from '../config/theme';

type RequestOptions = "POST" | "GET";

class HttpService {
    request(method: RequestOptions, url: string, data = {}): Promise<any> {
        return new Promise(async (resolve: any, reject: any) => {
            let headers: any = {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            };
            let fetchOptions: any = {
                method: method,
                headers
            };
            fetchOptions['body'] = JSON.stringify(data);
            return fetch(url, fetchOptions)
                .then((res) => {
                    console.log(`%c Response request from ${url}: `, 'background: ' + theme.COLORS.notification.warningBg + '; color: #fff', res);
                    if (res.ok) {
                        let {status: statusCode} = res;
                        if (statusCode === 401) {
                            ///handle error
                        }
                    }
                    return res.json();
                }).then(data => {
                    console.log(`%c Data REST from ${url}: `, 'background: ' + theme.COLORS.notification.successBg + '; color: #fff', data);
                    if (data.errors) {
                        return reject(data)
                    } else {
                        return resolve(data)
                    }
                }).catch(e => {
                    console.log(`%c Error REST api ${url}: `, 'background: ' + theme.COLORS.notification.dangerBg + '; color: #fff', e);
                    return reject(e)
                })
        });

    }
}

export default new HttpService()
