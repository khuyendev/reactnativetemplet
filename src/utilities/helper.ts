import { Platform} from 'react-native';

export function isAndroid() {
    return Platform.OS === 'android';
}

export function isIos() {
    return Platform.OS === 'ios';
}
export function serializeJSON(json: any = {}): string {
    if (Object.keys(json).length === 0) {
        return '';
    } else {
        let parameters = Object.keys(json).filter(function (key) {
            return json[key] !== null;
        });
        return parameters.map(function (keyName) {
            if (json[keyName] !== null) {
                return encodeURIComponent(keyName) + '=' + encodeURIComponent(json[keyName]);
            }
            return null;
        }).join('&');
    }
}