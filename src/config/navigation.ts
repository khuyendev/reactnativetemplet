const screens = {
    LOGIN: 'login',
    HOME:'home',
    CHILD:'child',
    homeTabs: {
        TAB_ONE: 'TAB_ONE',
        TAB_TWO: 'TAB_TWO',
        TAB_THREE: 'TAB_THREE',
        TAB_FOUR: 'TAB_FOUR',
    }
};
export {
    screens
}