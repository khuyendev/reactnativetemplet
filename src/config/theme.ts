import {PixelRatio} from 'react-native';
import {isAndroid} from '../utilities/helper';

const pixelRatio = PixelRatio.get();
const tintColor = '#2f95dc';
const COLORS: any = {
    primary: '#0098DA',
    secondary: '#004a7d',
    statusBar: '#153A62',
    inactive: '#90A4AE',
    appBackground: isAndroid() ? '#f3f3f3' : '#E9E9EF',
    black: '#444',
    white: '#ffffff',
    text: '#333',
    textInactive: '#6C7B88',
    placeholder: 'rgba(108, 123, 138, 0.6)',
    highlight: '#B0ADF3',
    divider: '#E8EAEC',
    buttonBackground: '#10ADE4',
    tabIndicator: '#fff',
    notification: {
        success: '#fff',
        successBg: '#3DAD48',
        statusSuccess: '#32913c',
        info: '#fff',
        infoBg: '#0091EA',
        statusInfo: '#007fce',
        danger: '#fff',
        dangerBg: '#FB6168',
        statusDanger: '#e2565d',
        warning: '#fff',
        warningBg: '#FFC107',
        statusWarning: '#e2ab04'
    },
    tintColor,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColor,
    tabBar: '#fefefe',
    errorBackground: 'red',
    errorText: '#fff',
    warningBackground: '#EAEB5E',
    warningText: '#666804',
    noticeBackground: tintColor,
    noticeText: '#fff',
};

const backIcon = isAndroid() ? 'arrow-left' : 'chevron-left';

const SIZES: any = {
    textSize:16,
    defaultPadding: 16
};

export const theme: any = {
    COLORS,
    SIZES,
    PIXEL_RATIO: pixelRatio,
    backIcon
};
