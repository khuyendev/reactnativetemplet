const dev = __DEV__;
export const BASE_URL = dev ? 'http://ec2-13-251-156-187.ap-southeast-1.compute.amazonaws.com/api' : 'http://ec2-13-251-156-187.ap-southeast-1.compute.amazonaws.com/api';

export const MAX_TIMEOUT_REQUEST = 10000;

export const GET_EXAMPLE_API = `${BASE_URL}/retailer`;
export const LOGIN_URL = dev ? `http://ec2-13-251-156-187.ap-southeast-1.compute.amazonaws.com/oauth/token` : `http://ec2-13-251-156-187.ap-southeast-1.compute.amazonaws.com/oauth/token`;
