const AppRoute = {
    SPLASH: 'SPLASH',
    LOGIN: 'LOGIN',
    LOGOUT: 'LOGOUT'
};
export {
    AppRoute
}