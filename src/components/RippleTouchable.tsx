import React from 'react';
import {View, TouchableOpacity, TouchableNativeFeedback} from 'react-native';
import {isAndroid} from '../utilities/helper';

interface Props {
  style?: any

  onPress?(): any

  onPressIn?(): any

  onPressOut?(): any

  onLongPress?(): any

  disabled?: boolean
}

class RippleTouchable extends React.Component<Props> {

  handlePress = () => {
    requestAnimationFrame(() => {
      this.props.onPress && this.props.onPress()
    })
  };

  handlePressIn = () => {
    requestAnimationFrame(() => {
      this.props.onPressIn && this.props.onPressIn()
    })
  };

  handlePressOut = () => {
    requestAnimationFrame(() => {
      this.props.onPressOut && this.props.onPressOut()
    })
  };

  handleLongPress = () => {
    requestAnimationFrame(() => {
      this.props.onLongPress && this.props.onLongPress()
    })
  };

  renderAndroidButton() {
    const {style, disabled, children} = this.props;
    return (
      <TouchableNativeFeedback
        onPress={this.handlePress}
        onPressIn={this.handlePressIn}
        onPressOut={this.handlePressOut}
        onLongPress={this.handleLongPress}
        accessibilityTraits={'button'}
        disabled={disabled}
      >
        <View style={style}>
          {children}
        </View>
      </TouchableNativeFeedback>
    )
  }

  renderIosButton() {
    return (
      <TouchableOpacity
        accessibilityTraits={'button'}
        activeOpacity={0.5}
        {...this.props}
      >
        {this.props.children}
      </TouchableOpacity>
    )
  }

  render() {
    return isAndroid() ? this.renderAndroidButton() : this.renderIosButton()
  }

}

export default RippleTouchable;
