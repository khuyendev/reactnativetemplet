import React from 'react';
import {View, StyleSheet} from 'react-native';
import {theme} from '../config/theme';
import LoadingIndicator from './LoadingIndicator';
import TextView from "./TextView";
import RippleTouchable from "./RippleTouchable";

interface Props {
  text?: string,
  refreshable?: boolean,

  onRefresh?(): any,
  loading?: boolean
}

function NoData({text, refreshable, onRefresh, loading}: Props) {

  let handleRefresh = () => {
    onRefresh && onRefresh();
  };

  return (
    <View style={styles.container}>
      <View style={{marginVertical: 8}}>
        {!!loading ? (
          <View style={{alignItems: 'center'}}>
            <LoadingIndicator/>
          </View>
        ) : (
          <TextView style={styles.text}>
            {text || 'Không có dữ liệu'}
          </TextView>
        )}
        {!!refreshable ? (
          <RippleTouchable
            onPress={handleRefresh}
          >
            <View style={styles.reloadContainer}>
              {/*<Icon*/}
                {/*name={'refresh'}*/}
                {/*color={theme.COLORS.primary}*/}
                {/*size={16}*/}
              {/*/>*/}
              <TextView style={styles.textReload}>
                {' Nhấn để tải lại'}
              </TextView>
            </View>
          </RippleTouchable>
        ) : <View/>}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 8,
  },
  text: {
    fontWeight: '600',
    fontSize: 14,
    color: theme.COLORS.inactive,
    textAlign: 'center'
  },
  reloadContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 8
  },
  textReload: {
    color: theme.COLORS.primary,
  }
});

export default NoData;
