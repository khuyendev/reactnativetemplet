import React from 'react';
import {FlatList, RefreshControl, View} from 'react-native';

import NoData from './NoData';
import {theme} from '../config/theme';
import LoadingIndicator from './LoadingIndicator';

interface Props {
    data: any[],
    renderItem: any,
    noDataText?: string,
    ListHeaderComponent?: any,
    scrollEnabled?: boolean,
    noDataRefreshable?: boolean,

    onEnd?(): any

    onNoDataRefresh?(): any

    [key: string]: any,

    customStyle?: any
}

class List extends React.PureComponent<Props, {}> {

    onEndReachedCalledDuringMomentum: boolean;
    list: any;

    constructor(props: Props) {
        super(props);
        this.onEndReachedCalledDuringMomentum=false;
        this.keyExtractor = this.keyExtractor.bind(this);
        this.handleRefresh = this.handleRefresh.bind(this);
        this.handleEndReached = this.handleEndReached.bind(this);
        this.renderFooter = this.renderFooter.bind(this);
        this.onMomentumScrollBegin = this.onMomentumScrollBegin.bind(this);
        this.handleNoDataRefresh = this.handleNoDataRefresh.bind(this);
        this.renderNoData = this.renderNoData.bind(this);
        this.scrollToFooter = this.scrollToFooter.bind(this);
    }

    keyExtractor = (item: any, i: any): string => i;

    handleRefresh() {
        this.props.onRefresh && this.props.onRefresh()
    }

    handleEndReached() {
        if (!this.onEndReachedCalledDuringMomentum) {
            this.props.onEnd && this.props.onEnd();
            this.onEndReachedCalledDuringMomentum = true;
        }
    }

    handleNoDataRefresh() {
        let {onNoDataRefresh} = this.props;
        onNoDataRefresh && onNoDataRefresh()
    }

    onMomentumScrollBegin() {
        this.onEndReachedCalledDuringMomentum = false
    }

    scrollToFooter() {
        this.list && this.list.scrollToEnd({animated: true});
    }

    scrollToTop() {
        this.list && this.list.scrollTo({y: 0, animated: true});
    }

    renderFooter() {
        let {loading} = this.props;
        if (loading !== undefined && !!loading) {
            return (
                <View style={{alignItems: 'center', marginVertical: 8}}>
                    <LoadingIndicator/>
                </View>
            )
        }
        return (
            <View/>
        )

    }

    renderNoData() {
        let {noDataText, noDataRefreshable} = this.props;
        return (
            <NoData
                text={noDataText}
                refreshable={noDataRefreshable}
                onRefresh={this.handleNoDataRefresh}
            />
        )
    }

    render(): JSX.Element {
        let {data, ListHeaderComponent, refreshing, customStyle} = this.props;
        let contentContainerStyle: any = {};
        let hasData = data.length !== 0;
        if (!hasData) {
            contentContainerStyle['flex'] = 1;
            contentContainerStyle['alignItems'] = 'center';
            contentContainerStyle['justifyContent'] = 'center';
        }
        return (
            <FlatList
                ref={list => this.list = list}
                contentContainerStyle={typeof ListHeaderComponent === 'undefined' && !hasData ? [contentContainerStyle, customStyle] : customStyle}
                data={data}
                keyExtractor={this.keyExtractor}
                onEndReached={this.handleEndReached}
                onEndReachedThreshold={0.5}
                onMomentumScrollBegin={this.onMomentumScrollBegin}
                ListEmptyComponent={this.renderNoData}
                ListFooterComponent={this.renderFooter}
                showsVerticalScrollIndicator={false}
                refreshControl={
                    <RefreshControl
                        refreshing={!!refreshing}
                        colors={[theme.COLORS.primary, theme.COLORS.buttonBackground, theme.COLORS.notification.successBg, theme.COLORS.notification.dangerBg]}
                        tintColor={theme.COLORS.buttonBackground}
                        onRefresh={this.handleRefresh}
                    />
                }
                keyboardShouldPersistTaps={'handled'}
                {...this.props}
            />
        );
    }
}

export default List;
