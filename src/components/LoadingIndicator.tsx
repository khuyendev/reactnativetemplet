import React from 'react';
import {ActivityIndicator} from 'react-native';
import {theme} from '../config/theme';

class LoadingIndicator extends React.PureComponent <any, any> {
  render(): JSX.Element {
    return (
      <ActivityIndicator
        animating
        color={theme.COLORS.primary}
      />
    );
  }
}

export default LoadingIndicator;
