import React from 'react';
import {Text, TextProperties, StyleSheet} from 'react-native';
import {theme} from '../config/theme';

class TextView extends React.Component<TextProperties> {

  render() {
    return (
      <Text {...this.props} style={[styles.text, this.props.style]}>{this.props.children}</Text>
    )
  }

}

const styles = StyleSheet.create({
  text: {
    fontSize: theme.SIZES.textSize,
    fontFamily: 'space-mono'
  }
});

export default TextView;
