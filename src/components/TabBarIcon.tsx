import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {theme} from "../config/theme";

interface Props {
    focused?: boolean,
    name?: boolean,
}

class TabBarIcon extends React.Component<Props> {
    render() {
        return (
            <Icon
                name={this.props.name}
                size={26}
                style={{marginBottom: -3}}
                color={this.props.focused ? theme.COLORS.tabIconSelected : theme.COLORS.tabIconDefault}
            />
        );
    }
}
export default TabBarIcon;