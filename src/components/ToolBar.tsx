import React from 'react';
import {StyleSheet} from 'react-native';
import {theme} from '../config/theme';
import TextView from "./TextView";

class ToolBar extends React.Component<{ text: string }> {

  render() {
    return (
      <TextView style={styles.text}>{this.props.text}</TextView>
    )
  }

}

const styles = StyleSheet.create({
  text: {
    color: theme.COLORS.white,
    fontSize: 16,
    fontWeight: '700'
  }
});

export default ToolBar;
