import { LoginActionTypes } from '../actions/types'
const initialState: any = {
    reduxMgs: "Redux is Connected"
};
export default function (state = initialState, action: any) {
    switch (action.type) {
        case LoginActionTypes.LOGIN:
            return Object.assign({}, state, {
                logging: true
            });
        case LoginActionTypes.LOGIN_SUCCESS:
            return Object.assign({}, state, {
                logging: false,
                data: action.data,
                message: action.message
            });
        case LoginActionTypes.LOGIN_ERROR:
            return Object.assign({}, state, {
                logging: false,
                data: action.data,
                message: action.message
            });
        default:
            return state
    }
}