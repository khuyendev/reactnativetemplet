import { createStore,applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga';
import logger from 'redux-logger';
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' 
import rootReducers from './reducers'
import rootSaga from './sagas';

//persitsInit
const persistConfig = {
  key: 'framgia@framgia.com',
  debug: __DEV__,
  whitelist: [],
  storage,
};

const sagaMiddleware = createSagaMiddleware();

const middleware = [ sagaMiddleware];

__DEV__ && middleware.push(logger);

const reducer = persistReducer(persistConfig, rootReducers);

const store = createStore(reducer, compose(
  applyMiddleware(...middleware)
));

let persistor = persistStore(store);

sagaMiddleware.run(rootSaga);

export {
  store,
  persistor
}