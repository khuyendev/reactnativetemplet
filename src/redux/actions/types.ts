const LoginActionTypes = {
    LOGIN: '@Login.ts/LOGIN',
    LOGIN_SUCCESS: '@Login.ts/LOGIN_SUCCESS',
    LOGIN_ERROR: '@Login.ts/LOGIN_ERROR',
    LOG_OUT: '@Login.ts/LOG_OUT',
};
export {
    LoginActionTypes
}