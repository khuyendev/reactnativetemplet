import {LoginActionTypes} from './types'
export function requestLogin(id: string, password: string) {
    return {
      type: LoginActionTypes.LOGIN,
      id,
      password
    }
  }