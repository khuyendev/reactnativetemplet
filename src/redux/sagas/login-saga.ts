import { all, call, fork, put, takeLatest } from 'redux-saga/effects';
import { LoginActionTypes } from '../actions/types';
import HttpService from '../../services/HttpService';
import { LOGIN_URL } from '../../config/api';
import NavigationService from "../../services/NavigationService";
import {AppRoute} from "../../config/route";

function* login() {
    yield takeLatest(LoginActionTypes.LOGIN, function* (action: any) {
        try {
            const data = yield call(HttpService.request, 'POST', LOGIN_URL, {
                grant_type: 'password',
                username: action.id,
                password: action.password,
                client_id: '1',
                client_secret: 'UlNRRqtzEVdZ7sXKzHFCvF311ZyJ06EFdO807YYo',
                // client_secret: 'Wzw3g5vwuGanigm1HhVwm7U4aBxXoPn54ssPQsWb',
                scope: '*'
            });
            if (data && !data.error) {
                yield put({
                    type: LoginActionTypes.LOGIN_SUCCESS,
                    message: 'Đăng nhập thành công',
                    data
                });
            } else {
                yield put({
                    type: LoginActionTypes.LOGIN_ERROR,
                    message: data.message || 'Any mgs ...'
                })
            }
        } catch (e) {
            yield put({
                type: LoginActionTypes.LOGIN_ERROR,
                message: e.message || e.toString()
            })
            NavigationService.navigate(AppRoute.LOGIN);
        }
    })
}

export default function* () {
    yield all([
        fork(login)
    ])
}
