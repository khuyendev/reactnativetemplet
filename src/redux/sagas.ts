import {all} from 'redux-saga/effects';
import loginSaga from './sagas/login-saga';

export default function* rootSaga() {
  yield all([
    loginSaga()
  ])
}
