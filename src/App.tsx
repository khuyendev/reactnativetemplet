import * as React from 'react';
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { persistor, store } from './redux/configureStore';
import AppContainer from './screens/AppNavigation';
import NavigationService from "./services/NavigationService";

export default class App extends React.Component<{}> {
    render() {
        return (
            <Provider store={store}>
                <PersistGate
                    persistor={persistor}
                >
                    <AppContainer ref={(ref: any) => {
                        NavigationService.setTopLevelNavigator(ref);
                    }} />
                </PersistGate>
            </Provider>
        );
    }
}